<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Никита Дубинский 181-321, лабораторная работа</title>
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <script type="text/javascript" src="js.js"></script>
</head>
<body>
<header>
    <img src="img/logo.png" alt="Логотип">
    <div>
        <p>Лабораторные работы по программированию веб-приложений</p>
        <p>Дубинский Никита, 181-321</p>
    </div>
</header>
<main>

    <?php
    if ( isset($_POST['A']) ) {
        $out_text = "<p>ФИО: ".$_POST['fio']."</p><br>
                         <p>Номер группы: ".$_POST['group']."</p><br>
                         <p>О себе: ".$_POST['about']."</p><br><p>Задача: ";
        switch ($_POST['option']) {
            case 's1':
                if (($_POST['A']+$_POST['B']>$_POST['C']) AND ($_POST['A']+$_POST['C']>$_POST['B']) AND ($_POST['B']+$_POST['C']>$_POST['A'])) {
                    $out_text.="ПЛОЩАДЬ ТРЕУГОЛЬНИКА";
                    $pp = ($_POST['A'] + $_POST['B'] + $_POST['C'])/2;
                    $result = sqrt($pp*($pp - $_POST['A'])*($pp - $_POST['B'])*($pp - $_POST['C']));
                } else {
                    $out_text.='НЕВЕРНОЕ УСЛОВИЕ';
                    $error=true;
                } break;
            case 's2':
                if (($_POST['A']+$_POST['B']>$_POST['C']) AND ($_POST['A']+$_POST['C']>$_POST['B']) AND ($_POST['B']+$_POST['C']>$_POST['A'])) {
                    $out_text.="ПЕРИМЕТР ТРЕУГОЛЬНИКА";
                    $result = $_POST['A'] + $_POST['B'] + $_POST['C'];
                } else {
                    $out_text.='НЕВЕРНОЕ УСЛОВИЕ';
                    $error=true;
                } break;
            case 's3':
                $out_text.='ОБЪЕМ ПАРАЛЛЕЛЕПИПЕДА';
                $result = $_POST['A'] * $_POST['B'] * $_POST['C'];
                break;
            case 's4':
                $out_text.='СРЕДНЕЕ АРИФМЕТИЧЕСКОЕ';
                $result = (($_POST['A']+$_POST['B']+$_POST['C'])/3);
                break;
            case 's5':
                $out_text.='ПРОИЗВЕДЕНИЕ КОРНЕЙ КВАДРАТНОГО УРАВНЕНИЯ';
                $result = $_POST['C'];
                break;
            case 's6':
                $out_text.='СУММА КОРНЕЙ КВАДРАТНОГО УРАВНЕНИЯ';
                $result = (-1) * $_POST['B'];
                break;
        }
        $out_text.="</p><br><p>A, B, C: ".$_POST['A'].", ".$_POST['B'].", ".$_POST['C']."</p><br>";
        $out_text.="<p>Ваш ответ:".$_POST['answer']."</p><br><p>Правильный ответ:".$result."</p>";

        if (!$error) {
            if ($result == $_POST['answer']) {
                $out_text.="Тест пройден";
            } else {$out_text.="Ошибка: тест не пройден";}
        } else {$out_text.="Ошибка: неверное условие";}
        if ($_POST['view'] == 'v1') {$out_text.="<a href='?n=".$_POST['fio']."&g=".$_POST['group']."' class='b'>Пройти ещё раз </a>";}
    
    echo "$out_text";

    if (array_key_exists('send_mail', $_POST)) {
        mail ($_POST['mail'], 'Результат теста', str_replace('<br>', "\r \n", $out_text), "From: test_dubinsky@gmail.com\n"."Content-Type: charset=utf-8\n");

        echo "<br><br><p>Результаты были отправлены на почту".$_POST['email']."</p>";}
    } else {
        $A = rand(10, 1000)/10;
        $B = rand(10, 1000)/10;
        $C = rand(10, 1000)/10;
        echo
            "<legend>Ввод данных пользователя</legend>
        <form method='POST' action=''>
            <label for='fio'>ФИО:</label>
            <input type='text' name='fio' id='fio' value='".$_GET['n']."'><br>
            <label for='group'>Номер группы:</label>
            <input type='text' name='group' id='group' value='".$_GET['g']."'><br>
            <label for='A'>Значение A:</label>
            <input type='text' name='A' id='A' value='$A'><br>
            <label for='B'>Значение В:</label>
            <input type='text' name='B' id='B' value='$B'><br>
            <label for='C'>Значение С:</label>
            <input type='text' name='C' id='C' value='$C'><br>
            <label for='answer'>Ваш ответ:</label>
            <input type='number' name='answer' id='answer'><br>
            <label for='about'>Немного о себе:</label>
            <textarea name='about' id='about'></textarea><br>
    
            <label for='option'>Тип задачи</label>
            <select name='option' id='option'>
                <option value='s1'>Площадь треугольника</option>
                <option value='s2'>Периметр треугольника</option>
                <option value='s3'>Объем параллелипипеда</option>
                <option value='s4'>Среднее арифметическое</option>
                <option value='s5'>Произведение корней квадратного уравнения</option>
                <option value='s6'>Сумма корней квадратного уравнения</option>
            </select><br>
    
            <input name='send_email' value='true' class='text' type='checkbox' id='email_answ'  onclick='check()'><label class='email_answ' for='email_answ'  onmouseenter='check()' >Отправить результаты на e-mail</label><br>
            
            <div id='if_js'><label class='text' for='email'>Ваш e-mail</label><input type='text' name='email' id='email'><br></div>
            <input class='text' type='radio' id='v1' checked name='view' value='v1'><label class='email_answ' for='v1'>Версия для просмотра в браузере</label><br>
            <input name='view' value='v2' class='text' type='radio'  id='print_view'>
            <label class='email_answ' for='print_view'>Версия для печати</label>
    
            <input type='submit' value='Проверить'><br>
        </form>";
    }
    ?>

</main>
<footer>
    <p>Лабораторные работы</p>
    <p>2019 год</p>
</footer>
<script>
    obj=document.getElementById('if_js');
function check() {
    if( document.getElementById('email_answ').checked ) {
    obj.style.display='block';
    console.log('check!');
    }
    else {
    obj.style.display='none';
    console.log('UNcheck!');
    }
}
</script>
</body>
</html>