 <?php
	$title = 'Никита Дубинский 181-321, лабораторная работа. Простейшая программа на PHP.
Конвертация статического контента в динамический.';

	if( date('s') % 2 === 0 ) 
		$name='img/photo1.png'; 
	else 
		$name='img/photo2.png';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo "$title" ?></title>
	<link rel="stylesheet" href="main.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
	<header>
		<p>Блог о путешествиях</p>
		<ul>
			<li><a href="<?php 
						$namelink = 'Главная'; 
						$link = 'index.php'; 
						$current_page = true; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a></li>
			<li><a href="<?php 
						$namelink = 'Где мы были'; 
						$link = 'page2.php'; 
						$current_page = false; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
			</li>
			<li><a href="<?php 
						$namelink = 'Россия'; 
						$link = 'page3.php'; 
						$current_page = false; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
		</ul>
	</header>

	<section id="main" style="background: url(<?php echo "$name" ?>) no-repeat center top / cover;">
		<div>
			<h1>Блог о путешествиях</h1>
			<p>TRAVEL. LOVE. BE INSPIRED</p>
		</div>
	</section>

	<section id="text">
		<div class="article">
			<h2>Галапагосские острова. Затерянный мир.</h2>
			<img src="img/foto1.jpg">
			<p>Галапагосы - одно из самых сильных впечатлений последних лет. В моем личном рейтинге эти острова на одном уровне с боливийским Альтиплано и африканским сафари. Уникально, необычно, другая планета и все путешествие насыщено моментами, когда не веришь своим глазам. 
Затерянный мир, где морские котики вповалку спят на скамейках на набережных, огромные игуаны вальяжно прогуливаются по городу, черепахи плавают с тобой бок о бок. А голубоногие олуши безмятежно высиживают яйца и исполняют свой знаменитый танец соблазнителей прямо в метре от тебя.</p>
			<i class="fab fa-vk"></i>
			<i class="fab fa-instagram"></i>
		</div>

		<div class="article">
			<h2>Мой бюджет</h2>
			<img src="img/foto2.jpg">
			<p>Меня часто спрашивают, сколько я трачу на путешествия. Сегодня я хочу представить вам мою бюджет на одно путешествие.</p>
			<table>
				<?php echo "<tr><th>Ячейка 1</th>
    <th>Ячейка 2</th><th>Ячейка 3</th></tr>" ?>
    			<tr>
    				<?php echo "<th>Ячейка 1</th>
    <th>Ячейка 2</th><th>Ячейка 3</th>"?>
    			</tr>
			</table>
			<i class="fab fa-vk"></i>
			<i class="fab fa-instagram"></i>
		</div>
	</section>
	<footer>
		<p>Сформировано <?php echo date("d.m.Y в H-i:s");?></p>
		<p>Блог о путешествиях</p>
		<p>2019 год</p>
	</footer>
</body>
</html>

