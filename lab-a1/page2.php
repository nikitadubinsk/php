<?php
	$title = 'Никита Дубинский 181-321, лабораторная работа. Простейшая программа на PHP.
Конвертация статического контента в динамический.';

	if( date('s') % 2 === 0 ) 
		$name2='img/m1.jpg'; 
	else 
		$name2='img/m2.jpg';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo "$title" ?></title>
	<link rel="stylesheet" href="main.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
	<header>
		<p>Блог о путешествиях</p>
		<ul>
			<li><a href="<?php 
						$namelink = 'Главная'; 
						$link = 'index.php'; 
						$current_page = false; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
			<li><a href="<?php 
						$namelink = 'Где мы были'; 
						$link = 'page2.php'; 
						$current_page = true; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
			</li>
			<li><a href="<?php 
						$namelink = 'Россия'; 
						$link = 'page3.php'; 
						$current_page = false; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
		</ul>
	</header>

	<section id="map">
		<h1>Гды мы были?</h1>
		<p>Список мест, которые мы посетили</p>
	</section>

	<section id="map_items">
		<h2>Список наших мест</h2>
		<div>
			<table>
					<?php echo "<tr><th>Страна</th>
	    <th>Город</th><th>Количество раз</th></tr>" ?>
	    			<tr>
	    				<?php echo "<th>Россия</th>
	    <th>Москва</th><th>5 раз</th>"?>
	    			</tr>
	    			<tr><th>Россия</th>
	    <th>Санкт-петербург</th><th>3 раза</th></tr>
	    <tr><th>Россия</th>
	    <th>Иваново</th><th>1 раз</th></tr>
			</table>
			<img src="<?php echo "$name2" ?>">
		</div>
	</section>

	<footer>
		<p>Сформировано <?php echo date("d.m.Y");?> в <?php echo date("H-i:s");?></p>
		<p>Блог о путешествиях</p>
		<p>2019 год</p>
	</footer>
</body>
</html>

