<?php
	$title = 'Никита Дубинский 181-321, лабораторная работа. Простейшая программа на PHP.
Конвертация статического контента в динамический.';

	if( date('s') % 2 === 0 ) 
		$name1='img/p1.jpg'; 
	else 
		$name1='img/p2.jpg';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo "$title" ?></title>
	<link rel="stylesheet" href="main.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
	<header>
		<p>Блог о путешествиях</p>
		<ul>
			<li><a href="<?php 
						$namelink = 'Главная'; 
						$link = 'index.php'; 
						$current_page = false; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
			<li><a href="<?php 
						$namelink = 'Где мы были'; 
						$link = 'page2.php'; 
						$current_page = false; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
			</li>
			<li><a href="<?php 
						$namelink = 'Россия'; 
						$link = 'page3.php'; 
						$current_page = true; 
						echo $link; 
						?>" <?php 
						if ($current_page) 
							echo ' class="selected_menu"'; ?>>
						<?php echo $namelink; ?>
				</a>
		</ul>
	</header>

	<section id="russia">
		<h1>Статьи про Россию</h1>
		<p>Самые лучшие места в самой лучшей стране</p>
	</section>

	<section id="text">
		<div class="article">
			<h2>Куршская коса</h2>
			<img src="<?php echo "$name1" ?>">
			<p>Куршская коса — это длинная узкая полоса суши с уникальным ландшафтом, флорой и фауной. Там произрастает около 600 видов растений и насчитывается 296 видов животных, а ещё проходит миграционный путь 150 видов птиц. За потрясающую природу Куршская коса включена в список Всемирного наследия ЮНЕСКО. Насладиться её красотами можно, пройдя по любому из шести пешеходных маршрутов одноимённого национального парка.</p>
			<i class="fab fa-vk"></i>
			<i class="fab fa-instagram"></i>
		</div>

		<div class="article">
			<h2>Хибины, Мурманская область</h2>
			<img src="img/p3.jpg">
			<p>Хибины — это горы на Кольском полуострове. Имеют мягкий рельеф и отличаются потрясающей природой. Там произрастает и обитает почти вся Красная книга. Большую часть года горный массив остаётся заснеженным, благодаря чему привлекает любителей горнолыжного спорта. Основные горнолыжные комплексы – на горах Айкуайвенчорр и Кукисвумчорр. Кататься можно с ноября по июнь.

Не стоите на лыжах? Отправляйтесь на джип-сафари по полуострову, рыбачьте в изумрудных озёрах или знакомьтесь с бытом кольских оленеводов. Также из программы-минимум в Хибинах — Полярно-альпийский ботанический сад и «Снежная деревня».</p>
			<i class="fab fa-vk"></i>
			<i class="fab fa-instagram"></i>
		</div>
	</section>

	<section id="contacts">
		<h2>Наши контакты</h2>
		<table>
				<?php echo "<tr><th>Телефон:</th>
    <th>8-(999)-999-99-99</th><th>41-42-43</th></tr>" ?>
    			<tr>
    				<?php echo "<th>Электронная почта:</th>
    <th>traval@gmail.com</th><th>add_traval@gmail.com (для рекламы)</th>"?>
    			</tr>
			</table>
	</section>

	

	<footer>
		<p>Сформировано <?php echo date("d.m.Y");?> в <?php echo date("H-i:s");?></p>
		<p>Блог о путешествиях</p>
		<p>2019 год</p>
	</footer>
</body>
</html>

