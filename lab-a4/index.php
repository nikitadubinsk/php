<?php
	$title = 'Никита Дубинский 181-321, лабораторная работа.';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo "$title" ?></title>
	<link rel="stylesheet" href="./style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
	<header>
		<img src="img/logo.png" alt="Логотип">
		<div>
			<p>Лабораторные работы по программированию веб-приложений</p>
			<p>Дубинский Никита, 181-321</p>
		</div>
	</header>
	<main>
		<?php
			$td = 5;
			function getTR( $data ) {
				$b;
				$col = $GLOBALS['td'];
     			$arr = explode( '*', $data );
     			for ($i=0; $i<count($arr); $i++) 
     				if ($arr[$i] == '') $b++;  

     			if ($b == count($arr)) return '0';
     			else {
	     			$ret = '<tr>';
					for($i=0; $i<$col; $i++) {
						if ($i < count($arr))
							$ret .= '<td>'.$arr[$i].'</td>'; 
						else $ret .= '<td></td>'; 
					}
					return $ret.'</tr>';
				}
			}

			function outTable($structure) {
				$c;
				$strings = explode( '#', $structure );
				if ((count($strings) == 1) && ($strings[0] == '')) $c='1';
				$datas='';
				for($i=0; $i<count($strings); $i++ ){
					if ($c == '1') echo 'Нет строк';
						else if (getTR($strings[$i]) == '0') echo 'Нет строк с ячейками';
							else $datas .= getTR( $strings[$i] );
				}
				echo '<table>'.$datas.'</table>';
			}

			if ($td <= 0) 
				echo 'Нет колонок';
			else {
				$structure=array( '*', '', 'C7*C8*C9#C10*C11*C12', 'C1*C2*C3*C4*C5*C6#C7*C8*C9#C10', 'C4*C5' );
				for($i=0; $i<count($structure); $i++) {
						echo '<h2>Таблица #'.($i+1).'</h2>';
						outTable($structure[$i]);
				}
			}
		?>
	</main>
	<footer>
		<p>Лабораторные работы</p>
		<p>2019 год</p>
	</footer>
</body>
</html>

