<?php
	$title = 'Никита Дубинский 181-321, лабораторная работа.';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo "$title" ?></title>
	<link rel="stylesheet" href="./style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
	<header>
		<img src="img/logo.png" alt="Логотип">
		<div>
			<p>Лабораторные работы по программированию веб-приложений</p>
			<p>Дубинский Никита, 181-321</p>
		</div>
	</header>
	<main>
		<?php 
			$x = 24;
			$type = 'E';
			$count = 5;
			$step = 0;

			if ($type == 'B') {
				echo '<ul>';
			} else if ($type == 'C') {
				echo '<ol>';
			} else if ($type == 'D') {
				echo '<table>';
			} 

			$max_stop = 10000;
			$min_stop = -10000;

			$min = 1000000;
			$max = -100000;
			$sum = null;
			$n = 0;

			for ($i = 0;  $i < $count; $i++) {

				if ($type == 'E') {
					echo '<div class="item">';
				}

				if ($x <= 10) {
					$f = 10*$x - 5;
				} else if (($x > 10) && ($x<20)) {
					$f = ($x + 3)*$x*$x;
				} else if ($x == 25) {
					$f = 'error';
					$n--;
				} else {
					$f = round(3/($x-25)+2, 3);
				}

				$n++;

				if ($type == 'A') {
					echo 'f('.$x.')='.$f; 
					if ( $i < $count - 1 ) echo '<br>'; 
				} else if ( ($type == 'B') || ($type == 'C')) {
					echo '<li>f('.$x.')='.$f.'</li>';
				} else if ($type == 'D') {
					echo '<tr>
							<th>'.$n.'.</th>
							<th>f('.$x.')</th>
							<th>'.$f.'</th>
						</tr>';
				} else if ($type == 'E') {
					echo 'f('.$x.')='.$f;
					echo '</div>';
				}

				if (($f < $min) & ($f != 'error')) {
					$min = $f;
				} 
				if (($f > $max) & ($f != 'error')) {
					$max = $f;
				}

				$sum = $sum + $f;

				if (($f >= $max_stop) || ($f <= $min_stop)) 
					break;

				$x = $x + $step;
			}

			if ($type == 'B') {
				echo '</ul>';
			} else if ($type == 'C') {
				echo '</ol>';
			} else if ($type == 'D') {
				echo '</table>';
			} 

			if ($min == 1000000) {
				$min = null;
			}

			if ($max == -100000) {
				$max = null;
			}

			echo '<br>';
			echo 'Минимальное значение: '.$min.'<br>';
			echo 'Максимальное значение: '.$max.'<br>';
			echo 'Сумма: '.$sum.'<br>';
			if ( $n==0 ) 
				echo 'Среднее арифметическое: ';
			else echo 'Среднее арифметическое: '.round($sum / $n, 3).'';
		?>
	</main>
	<footer>
		<p>Лабораторные работы</p>
		<p>2019 год</p>
		<?php echo 'Тип верстки - '.$type.''?>
	</footer>
</body>
</html>

