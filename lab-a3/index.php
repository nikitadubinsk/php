<?php
	$title = 'Никита Дубинский 181-321, лабораторная работа.';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo "$title" ?></title>
	<link rel="stylesheet" href="./style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
	<header>
		<img src="img/logo.png" alt="Логотип">
		<div>
			<p>Лабораторные работы по программированию веб-приложений</p>
			<p>Дубинский Никита, 181-321</p>
		</div>
	</header>
	<main>
		<div class="main">

			<?php
				$count = 0;
				if (!isset($_GET['store']))
					$_GET['store'] = '';
				else 
					if (isset($_GET['key'])) 
						$_GET['store'].=$_GET['key'];


			
					if (isset($_GET['taps'])) {
						$count = intval($_GET['taps']);
						$count++;
					}
					

				echo '<div class="result">'.$_GET['store'].'</div>';

				for ($i = 1; $i <= 10; $i++) {
					echo '<a class="number" href="?taps='.$count.'&key='.($i%10).'&store='.$_GET['store'].'">'.($i%10).'</a>';
					if ($i == 5) echo '<br>';
				}

				echo '<a class="exit" href="?taps='.$count.'&key=reset">СБРОС</a>';
			?>
		</div>
	</main>
	<footer>
		<p>Лабораторные работы</p>
		<p>2019 год</p>
		<?php 
			echo $count;
		?>
	</footer>
</body>
</html>

